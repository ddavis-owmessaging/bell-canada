#!/bin/bash

source /tormss02/imail/Mx8.0/.profile

#date=`TZ=GMT+8 date +%Y%m%d`
date=`date +%Y%m%d`
today=`date +%Y%m%d`
currentmonth=`date +%Y%m`

monthly_total=/home/reports/reportfn/monthly_dump_$currentmonth.out

total=`imfolderlist reportfn@toronto.rmgopenwave.com | grep "Message-Id" | tail -1 | awk -F":" '{print $1}'`
mail_output=/home/reports/reportfn/reportfn_all_mail.out
mail_output_clean=/home/reports/reportfn/reportfn_all_mail_clean.out
mail_sort=/home/reports/reportfn/reportfn_sort_mail.out
ip_sort=/home/reports/reportfn/reportfn_ip_sort.out
message=/home/reports/reportfn/reportfn_message_read.out
output=/home/reports/reportfn/reportfn_report_$date.txt
start=`cat /home/reports/reportfn/start.txt`
refresh=/home/reports/reportfn/clear.txt
newstart=`expr $total + 1` 


noro_list=/home/reports/reportfn/noro_ip.txt
noro_temp=/var/tmp/noro_temp_$date.txt
noro_report=/home/reports/reportfn/noro_count_report.csv

for ((a=start; a <= total ; a++))   
do
	immsgdump reportfn@toronto.rmgopenwave.com $a > $message
	mailaddr=`cat $message | grep "Return-Path:" | tail -1 | grep "@" | grep "<" | awk -F"<" '{print $2}' | cut -d ">" -f 1`

	if [ -z $mailaddr ]
	then
		mailaddr=`cat $message | grep "Return-Path:" | tail -1 | grep "@" | awk -F" " '{print $2}'`
	fi

	#mailaddr=`cat $message | grep "From:" | grep -v "aol.net" | grep "@" | grep "<" | grep -v "DIV" | tail -1 | awk -F"<" '{print $2}' | cut -d ">" -f 1` 
#	message_date=`cat $message | grep "Received-Date:" | awk -F" " '{print $4 "-" $3 "-" $5}'`
	#message_date=`cat message_read.out | grep "Date:" | grep "EDT" | awk -F" " '{print $4 "-" $3 "-" $5}'`

	mailip=`cat $message | grep "Received: from" | grep "toronto.rmgopenwave.com" | tail -1 | awk -F"[" '{print $2}' | cut -d "]" -f 1`

	if [ -z $mailip ]
	then
		mailip=`cat $message | grep "Received: from" | grep "toronto.rmgopenwave.com" | tail -1 | awk -F"(" '{print $2}' | cut -d ")" -f 1`
	fi

	printf "%s   %s \n" $mailaddr $mailip >> $mail_output

	for noro in `cat $noro_list`
	do
		count=0
		count=`cat $message | grep -i "$noro" | wc -l`

		if [ $count -ne 0 ]
		then
			echo "$noro" >> $noro_temp
		fi
	done

		
done

if [ -f $noro_temp ]
then

	count1=`cat $noro_temp | grep "64.34.220.122" | wc -l`
	count2=`cat $noro_temp | grep "64.34.220.123" | wc -l`
	count3=`cat $noro_temp | grep "64.34.220.124" | wc -l`
	count4=`cat $noro_temp | grep "76.74.139.250" | wc -l`
	count5=`cat $noro_temp | grep "76.74.139.251" | wc -l`
	count6=`cat $noro_temp | grep "76.74.139.252" | wc -l`
        count7=`cat $noro_temp | grep "76.74.139.223" | wc -l`

	printf "%s,%s,%s,%s,%s,%s,%s,%s \n" "$today" "$count1" "$count2" "$count3" "$count4" "$count5" "$count6" "$count7" >> $noro_report

	rm -rf $noro_temp

else
	echo "$today,0,0,0,0,0,0,0 " >> $noro_report
fi


#cat $noro_report | mailx -s "NORO - Report as SPAM count as of $today" aliant-asp-mon@openwave.com
uuencode $noro_report "noro_report_as_spam_report_$today.csv" | mailx -s "NORO - Report as SPAM count as of $today" aliant-asp-mon@openwave.com


clean=`cat $refresh`

if [ "$clean" == "YES" ]
then
	echo "0" > /home/reports/reportfn/start.txt
	imboxdelete tormss02 111111111111122
	echo "NO" > $refresh
else
	
	echo "$newstart" > /home/reports/reportfn/start.txt
fi

sed 's/
//g' $mail_output > $mail_output_clean

cat $mail_output_clean | sort | uniq -c | sort -k 1 -nr >> $mail_sort

cat $mail_output_clean | awk -F" " '{print $2}' | sort  | uniq -c | sort -k 1 -nr >> $ip_sort

cat $mail_output_clean >> $monthly_total

echo "Analysis of SPAM messages sent to reportfn@toronto.rmgopenwave.com for $today" > $output
echo "##################################################################" >> $output
echo " " >> $output
cat $mail_sort >> $output
echo " " >> $output
echo "##################################################################" >> $output
echo "Unique IPs from list:" >> $output
echo " " >> $output
cat $ip_sort >> $output

cat $output | mailx -s "DAILY - REPORTFN SPAM analysis for $today" aliant-asp-mon@openwave.com

rm -rf $mail_output
rm -rf $mail_sort
rm -rf $message
rm -rf $mail_output_clean
rm -rf $ip_sort



