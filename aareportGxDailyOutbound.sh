#!/bin/bash

source /tordir01/gx/.profile

mydate=`TZ=aaa24 date +%Y%m%d`
#mydate=`TZ=aaa24 date +20140922`

/tordir01/gx/bin/aareport -r all -group gx-out -csv -date  $mydate > /var/tmp/EdgeReport/aareportsoutbound_$mydate.csv
#/tordir01/gx/bin/aareport -r all -csv -date  20140922 > /var/tmp/EdgeReport/aareports_20140922.csv
sleep 15
cat /dev/null > /tordir01/gx/scripts/irftpdebug.out
cat /dev/null > /tordir01/gx/scripts/idrmail.scr
cd /var/tmp/EdgeReport
chmod 755 aareportsoutbound_$mydate.csv
chmod u+x /tordir01/gx/scripts/idrmail.scr
MAIL="/tordir01/gx/scripts/idrmail.scr"
MAILDEBUG="/tordir01/gx/scripts/idrmaildebug.out"

#FTP START
#Mike Added for FTP upload
rm /var/tmp/EdgeReport/upload/*
cp /var/tmp/EdgeReport/aareportsoutbound_$mydate.csv /var/tmp/EdgeReport/upload/
ftp -v ftp.openwave.com </tordir01/gx/scripts/ftp.scr >/tmp/ftpout
mailx -s "AAREPORT DAILY REPORT sent to ftp server from tordir01" aliant-asp-ops@owmessaging.com </tmp/ftpout
#FTP DONE

SEND="uuencode /var/tmp/EdgeReport/aareportsoutbound_$mydate.csv aareportsoutbound_$mydate.csv | mailx -s 'DAILY OUTBOUND AAREPORT - $mydate ' aliant-asp-mon@owmessaging.com kevin.cuba@owmessaging.com"
echo "$SEND" >> $MAIL
#$MAIL >> $MAILDEBUG 2>&1
$MAIL >> $MAILDEBUG
