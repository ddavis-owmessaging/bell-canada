#!/bin/bash

source /tordir01/imail/Mx8.0/.profile

#####pasthour=`TZ=GMT+5 date +%Y%m%d%H`
pasthour=`TZ=GMT+6 date +%Y%m%d%H`
input=/home/reports/aamta_recipients/aamta_excess_recpt_$pasthour.out
body=/home/reports/aamta_recipients/aamta_body_$pasthour.txt
send=/home/reports/aamta_recipients/aamta_HOURLY_complete_$pasthour.txt

temp=/home/reports/aamta_recipients/aamta_hourly_ldap.out

count=`cat $input | wc -l`
if [ $count -eq 0 ]
then
	echo "NO ACCOUNTS FOUND THIS HOUR" | mailx -s "HOURLY AAMTA MTZ04 and MTZ05 Rejected Recipient counts for $pasthour" aliant-asp-mon@owmessaging.com	
	exit
fi

while read line
do
	addy=`echo $line | awk -F" " '{print $2}'`
	count=`echo $line | awk -F" " '{print $1}'`

	ldapsearch -D cn=root -w secret mail=$addy > $temp
	type=`cat $temp | grep "adminpolicydn" | awk -F"=" '{print $3}' | awk -F"," '{print $1}'`
	mailfrom=`cat $temp | grep "mailfrom=" | awk -F"=" '{print $2}'`
	if [ -z "$mailfrom" ]
	then
		mailfrom="DNE"
	fi
	forward=`cat $temp | grep "mailforwardingaddress=" | awk -F"=" '{print $2}'`
	if [ -z "$forward" ]
	then
		forward="DNE"
	fi
	reply=`cat $temp | grep "mailreplyto=" | awk -F"=" '{print $2}'`
	if [ -z "$reply" ]
	then
		reply="DNE"
	fi
	status=`cat $temp | grep "mailboxstatus=" | awk -F"=" '{print $2}'`

	printf "%s,%s,%s,%s,%s,%s,%s \n" $count $addy $type $status "$mailfrom" "$forward" "$reply"  >> $body	

done < "$input"

echo "Below are the counts of accounts that were rejected by AAMTA on tormtz04/tormtz05 for exceeding 20 recipients." >> $send
echo " " >> $send
echo "Fields are broken down by :" >> $send
echo "# of rejected messages, address, account type, mailboxstatus, mailfrom, mailforwardingaddress, mailreplyto" >> $send
echo "NOTE: If any values do not exist DNE will be populated" >> $send
echo "####################################################################################################################" >> $send
echo " " >> $send
cat $body >> $send

cat $send | mailx -s "HOURLY AAMTA MTZ04 and MTZ05 Rejected Recipient counts for $pasthour" aliant-asp-mon@owmessaging.com
#cat $send | mailx -s "HOURLY AAMTA MTZ04 and MTZ05 Rejected Recipient counts for $pasthour" duy.doan@owmessaging.com

rm -rf $body
rm -rf $input
rm -rf $temp
