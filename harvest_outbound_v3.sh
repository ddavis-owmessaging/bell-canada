#!/bin/bash

#date=`date +%Y%m%d`
date=`TZ=GMT+7 date +%Y%m%d`
#date=20091103


ip_all=/var/tmp/harvest_out_IP_output.out
#ip_count=/var/tmp/harvest_out_IP_count.out
ip_drop_count=/var/tmp/harvest_out_IP_drop_count.out
ip_drop_all=/var/tmp/harvest_out_IP_drop_output.out
input_unknown=/var/tmp/harvest_out_test_unknown.out
input_drop=/var/tmp/harvest_out_drop.out
report=/home/reports/harvest/TOP_harvest_report_$date.txt
#report=TOP_harvest_report_$date.txt
output_all=/var/tmp/harvest_ALL_IP_outbound_$date.txt

bad_ips=/var/tmp/bad_ips.out

for i in 02 04
do
        cat /imail/logs/current/torspm$i/log/aamta.torspm$i.$date*.log | fgrep -i "MtaRcptHarvesterDropped" >> $input_drop
	cat /imail/logs/current/torspm$i/log/aamta.torspm$i.$date*.log | egrep -i "Warn;AcctUnknownUser|Note;SmtpInvalidUserDomain" >> $input_unknown
done



cat $input_drop | awk -F"[" '{print $2}' | cut -d "]" -f 1 | sort | uniq -c | sort -k 1 -nr | head -20 > $ip_drop_count


cat $input_drop | awk -F"[" '{print $2}' | cut -d "]" -f 1 | sort | uniq -c | sort -k 1 -nr | awk -F" " '{print $2}' | head -10 > $bad_ips


while read line2
do

        IP=`echo $line2 | awk -F" " '{print $2}'`
	count=`cat $input_unknown | grep "$IP" | wc -l`

        mailaddr=`cat $input_unknown | grep "$IP" | grep "from=<" | awk -F":" '{print $2}' | awk -F"=" '{print $2}' | sort | uniq -c | sort -k 1 -nr`
	 
	 echo "########### $IP ###############" >> $output_all
	 echo "# of harvesting attempts before dropped: $count"	>> $output_all
        # printf "Count : %s    IP : %s \n " $count $IP >> $output_all
	 echo " " >> $output_all
	 echo "========Associated Mail address(es)======== " >> $output_all
	 printf "%s \n" "$mailaddr" >> $output_all
	 echo " " >> $output_all
	
	mailaddr2=`cat $input_drop | grep "$IP" | grep "from=<" | awk -F":" '{print $2}' | awk -F"=" '{print $2}' | sort | uniq`

	echo "========Dropped Mail address(es)======== " >> $output_all
	printf "%s \n" "$mailaddr2" >> $output_all
	echo " " >> $output_all

done < "$ip_drop_count"


echo "########OUTBOUND DAILY REPORT FOR $date########" > $report
echo " " >> $report
echo "Top 20 IPs Dropped for Harvesting: " >> $report
echo " " >> $report
cat $ip_drop_count >> $report
echo " " >> $report
echo "###############TOP 20 IP BREAK DOWN###############" >> $report
echo " " >> $report
cat $output_all >> $report
echo " " >> $report
echo "TOP 10 Offending IP Time of Infraction Breakdown (HHMM)" >> $report
echo " " >> $report
for x in `cat $bad_ips`; do echo "###$x###" >> $report; grep $x $input_drop | awk -F" " '{print $2}' | cut -c1,2,3,4 | sort | uniq -c >> $report; echo " " >> $report; done



#cat $report | mailx -s "Daily Outbound Harvestor Report - $date" duy.doan@owmessaging.com

cat $report | mailx -s "Daily Outbound Harvestor Report - $date" aliant-asp-mon@owmessaging.com
cat $report | mailx -s "Daily Outbound Harvestor Report - $date" abuse@aliant.ca

rm -rf $bad_ips
rm -rf $ip_all
rm -rf $ip_drop_count
rm -rf $ip_drop_all
rm -rf $input_unknown
rm -rf $output_all
rm -rf $input_drop
