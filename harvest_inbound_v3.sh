#!/bin/bash

#date=`date +%Y%m%d`
date=`TZ=GMT+7 date +%Y%m%d`
#date=20140922


ip_all=/var/tmp/harvest_IP_output.out
#ip_count=/var/tmp/harvest_IP_count.out
ip_drop_count=/var/tmp/harvest_IP_drop_count.out
ip_drop_all=/var/tmp/harvest_IP_drop_output.out
input_unknown=/var/tmp/harvest_test_unknown.out
input_drop=/var/tmp/harvest_drop.out
report=/home/reports/harvest/TOP_harvest_report_inbound_$date.txt
report_csv=TOP_harvest_report_inbound_$date.csv
output_all=/var/tmp/harvest_ALL_IP_inbound_$date.txt

bad_ips=/var/tmp/bad_ips_input.out

#el_output=/var/tmp/el_output.out
#el_send=/var/tmp/el_send.txt


for i in 01 03 05
do
        cat /imail/logs/current/torspm$i/log/aamta.torspm$i.$date*.log | fgrep -i "MtaRcptHarvesterDropped" >> $input_drop
	cat /imail/logs/current/torspm$i/log/aamta.torspm$i.$date*.log | egrep -i "Warn;AcctUnknownUser|Note;SmtpInvalidUserDomain" >> $input_unknown
done

#./harvest_IP_parser.pl $input_unknown
#cat $ip_all | sort | uniq -c | sort -k 1 -nr > $ip_count


cat $input_drop | awk -F"[" '{print $2}' | cut -d "]" -f 1 | sort | uniq -c | sort -k 1 -nr | head -20 > $ip_drop_count

cat $input_drop | awk -F"[" '{print $2}' | cut -d "]" -f 1 | sort | uniq -c | sort -k 1 -nr | awk -F" " '{print $2}' | head -10 > $bad_ips



while read line2
do

       # count=`echo $line2 | awk -F" " '{print $1}'`

        IP=`echo $line2 | awk -F" " '{print $2}'`
	count=`cat $input_unknown | grep "$IP" | wc -l`

        mailaddr=`cat $input_unknown | grep "$IP" | grep "from=<" | awk -F":" '{print $2}' | awk -F"=" '{print $2}' | sort | uniq -c | sort -k 1 -nr`
	 
	 echo "########### $IP ###############" >> $output_all
	 echo "# of harvesting attempts before dropped: $count"	>> $output_all
        # printf "Count : %s    IP : %s \n " $count $IP >> $output_all
	 echo " " >> $output_all
	 echo "========Mail address(es)======== " >> $output_all
	 printf "%s \n" "$mailaddr" >> $output_all
	 echo " " >> $output_all
#         if [ $IP = "24.222.0.30" ]
#         then
#		echo "########### $IP ###############" >> $el_output
#		echo "# of harvesting attempts before dropped: $count" >> $el_output
#		echo " " >> $el_output
#		echo "========Mail address(es)======== " >> $el_output
#		echo " " >> $el_output
#		printf "%s \n" "$mailaddr" >> $el_output
#		echo " " >> $el_output
#         fi

done < "$ip_drop_count"


echo "########INBOUND DAILY REPORT FOR $date########" > $report
echo " " >> $report
echo "Top 20 IPs Dropped for Harvesting: " >> $report
echo " " >> $report
cat $ip_drop_count >> $report
echo " " >> $report
echo "###############TOP 20 IP BREAK DOWN###############" >> $report
echo " " >> $report
cat $output_all >> $report
echo " " >> $report
echo "TOP 10 Offending IP Time of Infraction Breakdown (HHMM)" >> $report
echo " " >> $report
for x in `cat $bad_ips`; do echo "###$x###" >> $report; grep $x $input_drop | awk -F" " '{print $2}' | cut -c1,2,3,4 | sort | uniq -c >> $report; echo " " >> $report; done
echo " " >> $report

#el_count=`cat $el_output | wc -l`

#if [ $el_count -gt 0 ]
#then
#	echo -e "EASTLINK INBOUND HARVESTOR STATS for $date" > $el_send
#	echo -e "This includes the count, IP address and unique list of mail addresses associated." >> $el_send
#	echo -e "#########################################################" >> $el_send
#	cat $el_output >> $el_send
#	cat $el_send | mailx -s "Daily Inbound Eastlink Harvestor Report - $date" duy.doan@owmessaging.com
#	cat $el_send | mailx -s "Daily Inbound Eastlink Harvestor Report - $date" Hughie.Cook@corp.eastlink.ca aliant-asp-mon@owmessaging.com
#fi

#cat $report | mailx -s "Daily Inbound Harvestor Report - $date" duy.doan@owmessaging.com
cat $report | mailx -s "Daily Inbound Harvestor Report - $date" aliant-asp-mon@owmessaging.com
cat $report | mailx -s "Daily Inbound Harvestor Report - $date" abuse@aliant.ca

#FTP START
#Mike Added for FTP upload
rm /home/reports/harvest/upload/*
cp $report /home/reports/harvest/upload/
mv /home/reports/harvest/upload/* /home/reports/harvest/upload/$report_csv 
ftp -v ftp.openwave.com </usr/local/scripts/ftp.scr >/tmp/ftpout
#mailx -s "INBOUND HARVESTER DAILY REPORT sent to ftp server from $HOSTNAME" aliant-asp-ops@owmessaging.com </tmp/ftpout
#FTP DONE


rm -rf $bad_ips
rm -rf $ip_all
rm -rf $ip_drop_count
rm -rf $ip_drop_all
rm -rf $input_unknown
rm -rf $output_all
rm -rf $input_drop
#rm -rf $el_output
#rm -rf $el_send
