#!/bin/bash 

pastday=`TZ=aaa24 date +%Y%m%d`
hostlist=/usr/local/scripts/massmail_host_list.txt
hostlist2=/usr/local/scripts/massmail_host_list2.txt
temp01=/var/tmp/mass_mail_input_$pastday.txt
temp02=/var/tmp/mass_mail_all_verdict_$pastday.txt
temp03=/var/tmp/mass_mail_origin_ip_$pastday.txt

limit=1500

output_all=/var/tmp/address_IP_list_DAILY.out
output_count=/var/tmp/address_count_DAILY.out
output_work=/var/tmp/output_work_DAILY.out
output_final=/var/tmp/massmail_presorted_DAILY_$pastday.txt
output_final_sorted=/var/tmp/massmail_sorted_DAILY_$pastday.txt

#output_spam1=/var/tmp/spam_only_DAILY.txt
#output_spam2=/var/tmp/spam_output_DAILY.txt
#output_spam3=/var/tmp/spam_send_DAILY.txt

for i in `cat $hostlist`
do
        for x in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
        do

                cat /imail/logs/current/$i/log/aamta.$i.$pastday$x*.log | grep "VoteServiceVerdict" >> $temp01
                cat /imail/logs/current/$i/log/aamta.$i.$pastday$x*.log | grep "X-Originating-IP" >> $temp03
        done

done

for j in `cat $hostlist2`
do
        for y in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
        do

                cat /imail/logs/current/$j/GX/log/aamta.$j.$pastday$y*.log | grep "VoteServiceVerdict" >> $temp01
                cat /imail/logs/current/$j/GX/log/aamta.$j.$pastday$y*.log | grep "X-Originating-IP" >> $temp03
        done

done

cat $temp01 | grep "action=[DROP][REJECT]" > $temp02 


/usr/local/scripts/address_IP_parser_new_daily.pl $temp01 $temp03

cat $output_all | sort | uniq -c > $output_count

while read line
do
	count=`echo $line | awk -F" " '{print $1}'`
	if [ $count -ge $limit ]
	then
		mail=`echo $line | awk -F" " '{print $2}'`
		orig_ip=`echo $line | awk -F" " '{print $3}'`
		printf "%s %s %s \n" $count $mail $orig_ip >> $output_work
	fi

done < "$output_count"

#while read line
#do
#	ip_check=`echo $line | awk -F" " '{print $3}'`
#	if [ -z $ip_check ]
#	then
#		spam_mail=`echo $line | awk -F" " '{print $2}'` 
	#	spam_count=`echo $line | awk -F" " '{print $1}'`
#		printf "%s \n" $spam_count $spam_mail >> $output_spam1
#	fi

#done < "$output_count"
		
while read line
do
	count=`echo $line | awk -F" " '{print $1}'`
	mail=`echo $line | awk -F" " '{print $2}'`
	orig_ip=`echo $line | awk -F" " '{print $3}'`
	spam_count=`cat $temp01 | grep "mailfrom=<$mail" | grep $orig_ip | grep "action=[DROP][REJECT]" | wc -l`

	#spam_count=`cat $temp01 | grep "mailfrom=<$mail" | grep "action=[DROP][REJECT]" | wc -l`

	if [ $spam_count -eq 0 ]
	then
		#spam_value="NOT_FLAGGED"
		spam_count="0"
		virus_count="0"
	else
		spam_count=`cat $temp02 | grep "mailfrom=<$mail" | grep $orig_ip | grep -v "CommTouchVirus,100,100" | wc -l`
		virus_count=`cat $temp02 | grep "mailfrom=<$mail" | grep $orig_ip | grep "CommTouchVirus,100,100" | wc -l`

		#spam_count=`cat $temp02 | grep "mailfrom=<$mail" | grep -v "CommTouchVirus,100,100" | wc -l`
		#virus_count=`cat $temp02 | grep "mailfrom=<$mail" | grep "CommTouchVirus,100,100" | wc -l`
		#spam_value="FLAGGED"
	fi

	if [ -z $orig_ip ]
	then
		dns_lookup="X-ORIGINATING-IP_NOT_FOUND"
	else

		dns_lookup=`nslookup $orig_ip | grep "name =" | awk -F"=" '{print $2}' | awk -F" " '{print $1}' | head -1`
	fi

	if [ -z $dns_lookup ]
	then
		dns_lookup="DNS_NOT_FOUND"
	fi
	printf "%s,%s,%s,%s,%s,%s \n" $count $mail $orig_ip $dns_lookup $spam_count $virus_count >> $output_final

done < "$output_work"
	
#for spam in `cat $output_spam1`
#do
#	spam_file_count=`cat $temp02 | grep "mailfrom=<$spam" | grep -v "CommTouchVirus,100,100" | wc -l`
#	virus_file_count=`cat $temp02 | grep "mailfrom=<$spam" | grep "CommTouchVirus,100,100" | wc -l`	
#	printf "%s,%s,%s \n" $spam $spam_file_count $virus_file_count >> $output_spam2

#done

echo "Count, Mail Address, From Host IP, DNS, # of SPAM Msgs, # of VIRUS Msgs" > $output_final_sorted
echo "==========================================================================================================" >> $output_final_sorted
	
sort -t "," -k 1 -nr $output_final >> $output_final_sorted

#echo "Mail Address, # of SPAM Msgs, # of VIRUS Msgs" > $output_spam3
#echo -e "==================================================================" >> $output_spam3
#cat $output_spam2 >> $output_spam3

#uuencode $output_final_sorted "mass_mailer_hourly_report_$pasthour.csv" | mailx -s "Hourly Mass Mailer report for $pasthour" duy.doan@owmessaging.com john.reali@owmessaging.com abuse@aliant.ca

#cat $output_final_sorted | mailx -s "Daily Mass Mailer report for $pastday" duy.doan@owmessaging.com john.reali@owmessaging.com abuse@aliant.ca gerard.white@bellaliant.ca aliant-asp-mon@owmessaging.com

cat $output_final_sorted | mailx -s "Daily Mass Mailer report for $pastday" abuse@aliant.ca
cat $output_final_sorted | mailx -s "Daily Mass Mailer report for $pastday" aliant-asp-mon@owmessaging.com
cat $output_final_sorted | mailx -s "Daily Mass Mailer report for $pastday" gerard.white@bellaliant.ca

#cat $output_spam3 | mailx -s "Daily Mass Mailer report SPAMMERS ONLY for $pastday" duy.doan@owmessaging.com john.reali@owmessaging.com
#cat $output_final_sorted | mailx -s "Hourly Mass Mailer report for $pasthour" duy.doan@owmessaging.com
#cat $output_spam3 | mailx -s "Hourly Mass Mailer report SPAMMERS ONLY $pasthour" duy.doan@owmessaging.com john.reali@owmessaging.com
#cat $output_spam3 | mailx -s "Hourly Mass Mailer report SPAMMERS ONLY $pasthour" duy.doan@owmessaging.com


rm $output_all
rm $output_count
rm $output_work
rm $output_final
rm $output_final_sorted
rm $temp01
rm $temp02
rm $temp03
#rm $output_spam1
#rm $output_spam2
#rm $output_spam3
