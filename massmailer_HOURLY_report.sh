#!/bin/bash 

if [ -f /var/tmp/mass_mailer_hourly.lck ]
then
        exit
else
        touch /var/tmp/mass_mailer_hourly.lck
#####pasthour=`TZ=GMT+5 date +%Y%m%d%H`
pasthour=`TZ=GMT+6 date +%Y%m%d%H`
hostlist=/usr/local/scripts/massmail_host_list.txt
hostlist2=/usr/local/scripts/massmail_host_list2.txt
temp01=/var/tmp/mass_mail_input_$pasthour.txt
temp02=/var/tmp/mass_mail_all_verdict_$pasthour.txt
temp03=/var/tmp/mass_mail_origin_ip_$pasthour.txt

limit=10

output_all=/var/tmp/address_IP_list.out
output_count=/var/tmp/address_count.out
output_work=/var/tmp/output_work.out
output_final=/var/tmp/massmail_presorted_$pasthour.txt
output_final_sorted=/home/reports/massmailer/massmailer_hourly_$pasthour.txt
output_dne=/var/tmp/output_dne.txt

#### Added for SPAM output
output_final_no_spam=/var/tmp/massmail_presorted_no_spam_$pasthour.txt
output_final_spam=/var/tmp/massmail_presorted_spam_$pasthour.txt
####

#output_spam1=/var/tmp/spam_only.txt
#output_spam2=/var/tmp/spam_output.txt
#output_spam3=/var/tmp/spam_send.txt

for i in `cat $hostlist`
do
        cat /imail/logs/current/$i/log/aamta.$i.$pasthour*.log | grep "VoteServiceVerdict" >> $temp01
	cat /imail/logs/current/$i/log/aamta.$i.$pasthour*.log | grep "X-Originating-IP" >> $temp03

done

for y in `cat $hostlist2`
do
        cat /imail/logs/current/$y/GX/log/aamta.$y.$pasthour*.log | grep "VoteServiceVerdict" >> $temp01
        cat /imail/logs/current/$y/GX/log/aamta.$y.$pasthour*.log | grep "X-Originating-IP" >> $temp03

done

cat $temp01 | grep "action=[DROP][REJECT]" > $temp02 


/usr/local/scripts/address_IP_parser_new.pl $temp01 $temp03

cat $output_all | sort | uniq -c > $output_count

while read line
do
	count=`echo $line | awk -F" " '{print $1}'`
	if [ $count -ge $limit ]
	then
		mail=`echo $line | awk -F" " '{print $2}'`
		orig_ip=`echo $line | awk -F" " '{print $3}'`
		printf "%s %s %s \n" $count $mail $orig_ip >> $output_work
	fi

done < "$output_count"

#while read line
#do
#	ip_check=`echo $line | awk -F" " '{print $3}'`
#	if [ -z $ip_check ]
#	then
#		spam_mail=`echo $line | awk -F" " '{print $2}'` 
	#	spam_count=`echo $line | awk -F" " '{print $1}'`
#		printf "%s \n" $spam_count $spam_mail >> $output_spam1
#	fi

#done < "$output_count"
		
while read line
do
	count=`echo $line | awk -F" " '{print $1}'`
	mail=`echo $line | awk -F" " '{print $2}'`
	orig_ip=`echo $line | awk -F" " '{print $3}'`
	spam_count=`cat $temp01 | grep "mailfrom=<$mail" | grep $orig_ip | grep "action=[DROP][REJECT]" | wc -l`

	#spam_count=`cat $temp01 | grep "mailfrom=<$mail" | grep "action=[DROP][REJECT]" | wc -l`

	if [ $spam_count -eq 0 ]
	then
		#spam_value="NOT_FLAGGED"
		spam_count="0"
		virus_count="0"
	else
		spam_count=`cat $temp02 | grep "mailfrom=<$mail" | grep $orig_ip | grep -v "CommTouchVirus,100,100" | wc -l`
		virus_count=`cat $temp02 | grep "mailfrom=<$mail" | grep $orig_ip | grep "CommTouchVirus,100,100" | wc -l`

		#spam_count=`cat $temp02 | grep "mailfrom=<$mail" | grep -v "CommTouchVirus,100,100" | wc -l`
		#virus_count=`cat $temp02 | grep "mailfrom=<$mail" | grep "CommTouchVirus,100,100" | wc -l`
		#spam_value="FLAGGED"
	fi

	if [ -z $orig_ip ]
	then
		dns_lookup="X-ORIGINATING-IP_NOT_FOUND"
	else

		dns_lookup=`nslookup $orig_ip | grep "name =" | awk -F"=" '{print $2}' | awk -F" " '{print $1}' | head -1`
	fi

	if [ -z $dns_lookup ]
	then
		dns_lookup="DNS_NOT_FOUND"
	fi
####	printf "%s,%s,%s,%s,%s,%s \n" $count $mail $orig_ip $dns_lookup $spam_count $virus_count >> $output_final


####Added for spam only look

        if [ $spam_count -eq 0 ]
        then
                printf "%s,%s,%s,%s,%s,%s \n" $count $mail $orig_ip $dns_lookup $spam_count $virus_count >> $output_final_no_spam
                printf "%s,%s,%s,%s,%s,%s \n" $count $mail $orig_ip $dns_lookup $spam_count $virus_count >> $output_final
        else
                printf "%s,%s,%s,%s,%s,%s \n" $count $mail $orig_ip $dns_lookup $spam_count $virus_count >> $output_final_spam
                printf "%s,%s,%s,%s,%s,%s \n" $count $mail $orig_ip $dns_lookup $spam_count $virus_count >> $output_final
        fi

####

done < "$output_work"
	
#for spam in `cat $output_spam1`
#do
#	spam_file_count=`cat $temp02 | grep "mailfrom=<$spam" | grep -v "CommTouchVirus,100,100" | wc -l`
#	virus_file_count=`cat $temp02 | grep "mailfrom=<$spam" | grep "CommTouchVirus,100,100" | wc -l`	
#	printf "%s,%s,%s \n" $spam $spam_file_count $virus_file_count >> $output_spam2

#done

echo "Below are the entries that were not found in Bell Aliant's allowed IP list:" >> $output_dne
echo " " >> $output_dne


while read line
do
        check=0
        check_total=0
        for i in `cat /home/reports/massmailer/aliant_ip_list.txt`
        do
                check=`echo $line | grep "$i" | wc -l`
                check_total=`expr $check_total + $check`
        done

        if [ $check_total -eq 0 ]
        then
                echo $line >> $output_dne
	fi

done < "$output_final"

####echo "Count, Mail Address, From Host IP, DNS, # of SPAM Msgs, # of VIRUS Msgs" > $output_final_sorted
echo "FLAGGED FOR SPAM - Count, Mail Address, From Host IP, DNS, # of SPAM Msgs, # of VIRUS Msgs" > $output_final_sorted
echo "==========================================================================================================" >> $output_final_sorted
	
####sort -t "," -k 1 -nr $output_final >> $output_final_sorted


## Added for Spam first
sort -t "," -k 1 -nr $output_final_spam >> $output_final_sorted
echo " " >> $output_final_sorted
echo " " >> $output_final_sorted
echo " " >> $output_final_sorted
echo "CLEAN MESSAGES - Count, Mail Address, From Host IP, DNS, # of SPAM Msgs, # of VIRUS Msgs" >> $output_final_sorted
echo "==========================================================================================================" >> $output_final_sorted
sort -t "," -k 1 -nr $output_final_no_spam >> $output_final_sorted
echo " " >> $output_final_sorted
echo " " >> $output_final_sorted

#echo "Mail Address, # of SPAM Msgs, # of VIRUS Msgs" > $output_spam3
#echo -e "==================================================================" >> $output_spam3
#cat $output_spam2 >> $output_spam3

echo " " >> $output_final_sorted
echo "==========================================================================================================" >> $output_final_sorted
echo " " >> $output_final_sorted
cat $output_dne >> $output_final_sorted

#uuencode $output_final_sorted "mass_mailer_hourly_report_$pasthour.csv" | mailx -s "Hourly Mass Mailer report for $pasthour" duy.doan@owmessaging.com john.reali@owmessaging.com abuse@aliant.ca

#cat $output_final_sorted | mailx -s "Hourly Mass Mailer report for $pasthour" duy.doan@owmessaging.com john.reali@owmessaging.com abuse@aliant.ca gerard.white@bellaliant.ca aliant-asp-mon@owmessaging.com
#cat $output_final_sorted | mailx -s "Hourly Mass Mailer report for $pasthour" duy.doan@owmessaging.com
cat $output_final_sorted | mailx -s "Hourly Mass Mailer report for $pasthour" aliant-asp-mon@owmessaging.com
cat $output_final_sorted | mailx -s "Hourly Mass Mailer report for $pasthour" abuse@aliant.ca
##cat $output_final_sorted | mailx -s "Hourly Mass Mailer report for $pasthour" gerard.white@bellaliant.ca
#cat $output_spam3 | mailx -s "Hourly Mass Mailer report SPAMMERS ONLY $pasthour" duy.doan@owmessaging.com john.reali@owmessaging.com
#cat $output_spam3 | mailx -s "Hourly Mass Mailer report SPAMMERS ONLY $pasthour" duy.doan@owmessaging.com


rm $output_all
rm $output_count
rm $output_work
rm $output_final
rm $output_dne
#rm $output_final_sorted
rm $temp01
rm $temp02
rm $temp03

#####removing spam messages
rm $output_final_spam
rm $output_final_no_spam 
#rm $output_spam1
#rm $output_spam2
#rm $output_spam3

rm /var/tmp/mass_mailer_hourly.lck

fi
