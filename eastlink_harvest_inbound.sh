#!/bin/bash

input_drop=/var/tmp/eastlink_harvest_drop.out
input_unknown=/var/tmp/eastlink_harvest_unknown.out

date_hour=`date +%H`

if [ $date_hour = 12 ]
then
	date=`date +%Y%m%d`
	output_all=/var/tmp/eastlink_harvest_ALL_IP_inbound_$date.txt
	report=/home/reports/harvest/eastlink_TOP_harvest_report_inbound_first_$date.txt

elif [ $date_hour = 00 ]
then
	date=`TZ=GMT+7 date +%Y%m%d`
	output_all=/var/tmp/eastlink_harvest_ALL_IP_inbound_$date.txt
	report=/home/reports/harvest/eastlink_TOP_harvest_report_inbound_all_$date.txt
fi



for i in 01 03 05
do
        cat /imail/logs/current/torspm$i/log/aamta.torspm$i.$date*.log | fgrep -i "MtaRcptHarvesterDropped" >> $input_drop
        cat /imail/logs/current/torspm$i/log/aamta.torspm$i.$date*.log | egrep -i "Warn;AcctUnknownUser|Note;SmtpInvalidUserDomain" >> $input_unknown
done

count=`cat $input_unknown | grep "24.222.0.30" | wc -l`
drop_count=`cat $input_drop | grep "24.222.0.30" | wc -l`

count1=`cat $input_unknown | grep "24.224.136.13" | wc -l`
drop_count1=`cat $input_drop | grep "24.224.136.13" | wc -l`

count2=`cat $input_unknown | grep "24.224.136.8" | wc -l`
drop_count2=`cat $input_drop | grep "24.224.136.8" | wc -l`

count3=`cat $input_unknown | grep "24.224.136.9" | wc -l`
drop_count3=`cat $input_drop | grep "24.224.136.9" | wc -l`

count4=`cat $input_unknown | grep "24.224.136.10" | wc -l`
drop_count4=`cat $input_drop | grep "24.224.136.10" | wc -l`

count5=`cat $input_unknown | grep "24.224.136.30" | wc -l`
drop_count5=`cat $input_drop | grep "24.224.136.30" | wc -l`

mailaddr=`cat $input_unknown | grep "24.222.0.30" | grep "from=<" | awk -F":" '{print $2}' | awk -F"=" '{print $2}' | sort | uniq -c | sort -k 1 -nr`
mailaddr1=`cat $input_unknown | grep "24.224.136.13" | grep "from=<" | awk -F":" '{print $2}' | awk -F"=" '{print $2}' | sort | uniq -c | sort -k 1 -nr`
mailaddr2=`cat $input_unknown | grep "24.224.136.8" | grep "from=<" | awk -F":" '{print $2}' | awk -F"=" '{print $2}' | sort | uniq -c | sort -k 1 -nr`
mailaddr3=`cat $input_unknown | grep "24.224.136.9" | grep "from=<" | awk -F":" '{print $2}' | awk -F"=" '{print $2}' | sort | uniq -c | sort -k 1 -nr`
mailaddr4=`cat $input_unknown | grep "24.224.136.10" | grep "from=<" | awk -F":" '{print $2}' | awk -F"=" '{print $2}' | sort | uniq -c | sort -k 1 -nr`
mailaddr5=`cat $input_unknown | grep "24.224.136.30" | grep "from=<" | awk -F":" '{print $2}' | awk -F"=" '{print $2}' | sort | uniq -c | sort -k 1 -nr`

echo "########### 24.222.0.30 ###############" >> $output_all
echo " " >> $output_all
echo "# of dropped messages from harvesting: $drop_count" >> $output_all
echo " " >> $output_all
echo "# of harvesting attempts: $count" >> $output_all
echo " " >> $output_all
echo "========Mail address(es)======== " >> $output_all
printf "%s \n" "$mailaddr" >> $output_all
echo " " >> $output_all
echo " " >> $output_all
echo "########### 24.224.136.13 ###############" >> $output_all
echo " " >> $output_all
echo "# of dropped messages from harvesting: $drop_count1" >> $output_all
echo " " >> $output_all
echo "# of harvesting attempts: $count1" >> $output_all
echo " " >> $output_all
echo "========Mail address(es)======== " >> $output_all
printf "%s \n" "$mailaddr1" >> $output_all
echo " " >> $output_all
echo " " >> $output_all
echo "########### 24.224.136.8 ###############" >> $output_all
echo " " >> $output_all
echo "# of dropped messages from harvesting: $drop_count2" >> $output_all
echo " " >> $output_all
echo "# of harvesting attempts: $count2" >> $output_all
echo " " >> $output_all
echo "========Mail address(es)======== " >> $output_all
printf "%s \n" "$mailaddr2" >> $output_all
echo " " >> $output_all
echo " " >> $output_all
echo "########### 24.224.136.9 ###############" >> $output_all
echo " " >> $output_all
echo "# of dropped messages from harvesting: $drop_count3" >> $output_all
echo " " >> $output_all
echo "# of harvesting attempts: $count3" >> $output_all
echo " " >> $output_all
echo "========Mail address(es)======== " >> $output_all
printf "%s \n" "$mailaddr3" >> $output_all
echo " " >> $output_all
echo " " >> $output_all
echo "########### 24.224.136.10 ###############" >> $output_all
echo " " >> $output_all
echo "# of dropped messages from harvesting: $drop_count4" >> $output_all
echo " " >> $output_all
echo "# of harvesting attempts: $count4" >> $output_all
echo " " >> $output_all
echo "========Mail address(es)======== " >> $output_all
printf "%s \n" "$mailaddr4" >> $output_all
echo " " >> $output_all
echo " " >> $output_all
echo "########### 24.224.136.30 ###############" >> $output_all
echo " " >> $output_all
echo "# of dropped messages from harvesting: $drop_count5" >> $output_all
echo " " >> $output_all
echo "# of harvesting attempts: $count5" >> $output_all
echo " " >> $output_all
echo "========Mail address(es)======== " >> $output_all
printf "%s \n" "$mailaddr5" >> $output_all
echo " " >> $output_all


if [ $date_hour = 12 ]
then
	date_report=`date +%Y%m%d`
	echo -e "EASTLINK INBOUND HARVESTOR STATS for first 12 hours of $date_report" > $report
	echo " " >> $report
	echo -e "This includes the count, IP address and unique list of mail addresses associated." >> $report
	echo -e "#########################################################" >> $report
	cat $output_all >> $report
	cat $report | mailx -s "Daily Inbound Eastlink Harvestor Report - 00:00-12:00 - $date_report" Kirk.MacDonald@corp.eastlink.ca aliant-asp-mon@owmessaging.com abuse@aliant.ca

elif [ $date_hour = 00 ]
then
	date_report=`TZ=GMT+7 date +%Y%m%d`
	echo -e "EASTLINK INBOUND HARVESTOR STATS for all of $date_report" > $report
	echo " " >> $report
	echo -e "This includes the count, IP address and unique list of mail addresses associated." >> $report
	echo -e "#########################################################" >> $report
	cat $output_all >> $report
	cat $report | mailx -s "Daily Inbound Eastlink Harvestor Report - Total - $date_report" Kirk.MacDonald@corp.eastlink.ca aliant-asp-mon@owmessaging.com abuse@aliant.ca

fi

rm -rf $input_drop
rm -rf $input_unknown
rm -rf $output_all
