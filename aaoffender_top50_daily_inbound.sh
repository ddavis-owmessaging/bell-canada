#!/bin/bash

source /tordir01/gx/.profile

mydate=`TZ=aaa24 date +%Y%m%d`
####reportdate=`TZ=aaa24 date +%m%d%Y`
reportdate=`TZ=aaa24 date +%Y%m%d`

output="/var/tmp/EdgeReport/aaoffenders_inbound_$reportdate.csv"
upload_dir=/var/tmp/EdgeReport/upload_inbound/
log=/var/tmp/gx_inbound_size_check.log


/tordir01/gx/bin/aaoffenders  -all -nodns -group gx-in -top 50 -date $mydate | tr '[:blank:]' '[,]' | grep -v "192.168.64.160" | grep -v "192.168.64.8" | grep -v "207.154.59.124" | grep -v "127.0.0.1" > $output


#Mail Send
cat $output | mailx -s "INBOUND AAOFFENDERS DAILY TOP 50 REPORT - $mydate" aliant-asp-ops@owmessaging.com

#FTP section
cd $upload_dir
rm *
cp $output $upload_dir
ftp -v ftp.openwave.com </tordir01/gx/scripts/ftp_inbound.scr >/tmp/ftpout

#Check size section

size=`ls -la $output | awk -F" " '{print $5}'`
if [ $size -eq 0 ]
then
	echo "##########`date`###########" >> $log
	echo "Size was 0. BB will be alarmed." >> $log
	echo `ls -la $output` >> $log
	echo "RED" > /tmp/bb_gx_inbound

else
	echo "##########`date`###########" >> $log
	echo "Size is not 0. All Clear." >> $log
	echo `ls -la $output` >> $log
	echo "GREEN" > /tmp/bb_gx_inbound

fi
