#!/bin/bash 

#pastday=`TZ=aaa24 date +%Y%m%d`
pastday=`date +%Y%m%d`
temp01=/var/tmp/input_all_nio_$pastday.out
ip_list=/var/tmp/ip_nio_list.out
output=/var/tmp/ip_nio_output.out
report=/home/reports/nio/nio_error_report_$pastday.txt


for hosts in 02 04
do
	cat /imail/logs/current/torspm$hosts/log/aamta.*$pastday*.log | grep "Note;NioMaxConnectionsFromIp" >> $temp01
done

cat $temp01 | awk -F" " '{print $9}' | awk -F"[" '{print $2}' | cut -d "]" -f 1 | sort | uniq > $ip_list

for ip in `cat $ip_list`
do
	echo "###### $ip ######" >> $output
	echo " " >> $output
	count=`cat $temp01 | grep "$ip" | wc -l` 
	echo "Total number of NioMaxconnection errors is $count" >> $output 
	echo " " >> $output
	echo "Errors broken down by time: " >> $output
	cat $temp01 | grep "$ip" | awk -F" " '{print substr($2,0,4)}' | sort | uniq -c >> $output
	echo " " >> $output

done


echo "Total IPs that have exceeded the connections threshold and have experienced NioMaxConnections errors on $pastday." > $report
echo " " >> $report
cat $output >> $report

cat $report | mailx -s "Total NioMaxConnection errors for outgoing traffic on $pastday" aliant-asp-mon@owmessaging.com

rm -rf $temp01
rm -rf $ip_list
rm -rf $output
