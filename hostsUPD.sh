#!/bin/bash
#
# bash script: hostsUPD.sh
#
# Usage: script <username> <password> </home/amesoner> </etc> 
#
# Real time: ./hostsUPD.sh amesoner <password> /home/amesoner /etc
#
# Dave Davis
# Date: 12-Oct 2012
#
# This script uses expect scripts within bash scripts.  Future editing needs care
# due to this embedded use on how regular expressions are passed back to the bash
# shell.  Any questions ask Ed K :)
#
 
SendHostsFile ( )
#--------------------------------------------------------------------------------------------
#
# SendHostsFile performs:
#
#   - scp is used to copy the pertinent VM hostfile to the VM in /etc path
#

{
    expect -c "
        log_user 0
        spawn \scp -q -o StrictHostKeyChecking=no $HOSTNAME.$j $username@$server:$destnpath

        expect {
            eof { send_user \"\tFailure: SCP Connection cannot be established\n\"; exit 1 }
            \"*assword\"
        }

        send \"$password\r\"

        expect {
            \"*\$ \"
        }

        send_user \"\tSCP successfully transferred file $HOSTNAME.$j to VM-$j in path $destnpath\n\"

        exit
        close

    " >> $LOGFILE
}
 
PermsCopyFile ( )
#--------------------------------------------------------------------------------------------
#
# PermsCopyFile performs:
#
#   - ssh connection to the VM
#   - sudo's to root
#   - chown root hosts
#   - chgrp root hosts
#   - copy /etc/hosts to /etc/hosts.<$START>
#

{
    expect -c "
        log_user 0
        spawn ssh -q -o StrictHostKeyChecking=no $username@$server

        expect {
            eof { send_user \"\nSSH failure to $server\n\"; exit 1 }
            \"*assword\"
        }

        send \"$password\r\"

        expect {
            \"*\$ \"
        }

        send \"sudo -i\r\"
        sleep 1

        expect {
            \"^\[sudo\]\"
        }

        send \"$password\r\"

        expect {
            \"*\# \"
        }

        send \"chown root $destnpath/hosts.$j\r\"
        sleep 1

        expect {
            \"*cannot*\" { send_user \"$destnpath/hosts.$j chown permission operation failed.\n\"; exit 1}
            \"*\# \"
        }

        send \"chgrp root $destnpath/hosts.$j\r\"
        sleep 1

        expect {
            \"*\# \"
        }

        send \"chmod 644 $destnpath/hosts.$j\r\"
        sleep 1

        expect {
            \"*\# \"
        }

        send \"cp -f $hostspath/$hostsname $hostspath/$hostsname.$START\r\"

        sleep 1

        expect {
           timeout { send_user \"\ncopy failed.\n\"; exit 1}
           \"*\# \"
        }

        send_user \"\tPermissions changed and hosts file copied successfully on VM-$j\n\"
        send \"exit\r\"
        close
    " >> $LOGFILE
}


MoveHostsFile ( )
#--------------------------------------------------------------------------------------------
#
# MoveHostsFile performs:
#
#   - ssh connection to the VM
#   - sudo's to root
#   - mv /home/amesoner/hosts.<vm #> to /etc/hosts 
#

{
    expect -c "
        log_user 0
        spawn ssh -q -o StrictHostKeyChecking=no $username@$server

        expect {
            eof { send_user \"\nSSH failure to $server\n\"; exit 1 }
            \"*assword\"
        }

        send \"$password\r\"

        expect {
            \"*\$ \"
        }

        send \"sudo -i\r\"
        sleep .5 

        expect {
            \"\r\n\"
        }

        send \"$password\r\"

        expect {
            \"\r\n\"
        }

        send \"mv -f $destnpath/hosts.$j $hostspath/$hostsname\r\"

        sleep .5 

        expect {
           timeout { send_user \"\ncopy failed.\n\"; exit 1}
           \"*cannot*\" { send_user \"$destnpath/hosts.$j move failed.\n\"; exit 1}
           \"*\# \"
        }

        send_user \"The file $destnpath/hosts.$j has been moved to $hostspath/$hostsname on VM-$j\n\"

        send \"exit\r\"
        close
    " >> $LOGFILE
}

LoadBase ( )
#--------------------------------------------------------------------------------------------
#
# LoadBase performs:
#
#   - Creates the first 10 lines of the hosts file 
#   - Populates the constant 4 lines including VM hostname and aliases 
#   - Populates a header containg write datestamp for comparsion use on creation 
#
{
    basefile="base.hosts"

    IFS=$(echo -en "\n\b")
    exec 3<&0
    exec 0<"$basefile"

    linecount=1

    while read -r line
    do
        if [ $linecount -eq 1 ] 
        then
            echo "#" >> $HOSTNAME.$j
            echo "# VM-$j hosts file" >> $HOSTNAME.$j
            echo "#" >> $HOSTNAME.$j
            echo "# Date-Timestamp Created: $START" >> $HOSTNAME.$j
            echo "#" >> $HOSTNAME.$j
            echo "" >> $HOSTNAME.$j
            echo "$line" >> $HOSTNAME.$j
            LoadFQDN $j
        else
            echo "$line" >> $HOSTNAME.$j
        fi

        let "linecount=linecount+1"
    done

    exec 0<&3
}

LoadMaster ( )
#--------------------------------------------------------------------------------------------
#
# LoadMaster performs:
#
#   - Opens the file master.hosts and copies the contents into the hosts.VM-n file 
#
{
    mastfile="master.hosts"

    IFS=$(echo -en "\n\b")
    exec 5<&0
    exec 0<"$mastfile"

    while read -r line
    do
        echo "$line" >> $HOSTNAME.$j
    done

    exec 0<&5
}

LoadFQDN ( )
#--------------------------------------------------------------------------------------------
#
# LoadFQDN performs:
#
#   - Searches file fqdn.hosts on the loop counter which matches the VM number
#     currently being used.
#   - Writes to the hosts.<loop counter> file the VM hostname and alias line 
#
{
    vmnumber="$1"
    let "vmnumber=vmnumber-10"
 
    fqdnfile="fqdn.hosts"

    IFS=$(echo -en "\n\b")
    exec 4<&0
    exec 0<"$fqdnfile"

    linecount=1

    while read -r line
    do
        if [ $vmnumber -eq $linecount ]
        then
            echo "$line" >> $HOSTNAME.$j
        fi
        let "linecount=linecount+1"
    done

    exec 0<&4
}

CheckCreatedFile ( )
#--------------------------------------------------------------------------------------------
#
# CheckCreatedFile performs:
#
#   - Checks that the newly created hosts file meets the criteria:
#        -> exists
#        -> is readable
#        -> contains the time/datestamp of the current create process 
#
{
    hostsfile="$HOSTNAME.$j"

    # make sure file exist and readable

    if [ ! -f $hostsfile ]; then
        echo -e "\t$hostsfile : does not exist" >> $LOGFILE
        exit 1
    elif [ ! -r $hostsfile ]; then
        echo -e "\t$hostsfile : cannot be read" >> $LOGFILE
        exit 2
    fi

    # Compare the creation date to validate new hosts file
    # is from this new build

    DT=$(grep $START $hostsfile)
    DTSTAMP=$(echo $DT | awk '{print $4 }')

    if [ "$DTSTAMP" == "$START" ]
    then 
        echo -e "\thosts file $HOSTNAME.$j created successfully" >> $LOGFILE
    else
        echo -e "\thost file creation for $HOSTNAME.$j has failed" >> $LOGFILE
    fi
}

CreateHostsFile ( )
#--------------------------------------------------------------------------------------------
#
# CreateHostsFile performs:
#
#   - Control of the hosts file creation procedure 
#
{
    LoadBase
    LoadMaster
    CheckCreatedFile
}


GetTmpFileData ( )

{
    flag="$1"

    if [ "$flag" == "copychk" ]
    then
        cfile="$destnpath/hosts.$j"
        COPYCHK="$CDIR/copychk.out"
    elif [ "$flag" == "movechk" ]
    then
        cfile="$hostspath/$hostsname"
        COPYCHK="$CDIR/movechk.out"
    fi
   
    echo "VM-$j" >> $COPYCHK
 
    expect -c "
        log_user 0
        spawn ssh -q -o StrictHostKeyChecking=no $username@$server

        expect {
            eof { send_user \"\nSSH failure to $server\n\"; exit 1 }
            \"*assword\"
        }

        send \"$password\r\"

        expect {
            \"*\$ \"
        }

        send \"ls -la $cfile\r\"

        expect -re \"\n(\[^\r]*)\r\"

        log_user 1

        set fsize [lrange \$expect_out(1,string) 4 4]
        puts \"Size........: \$fsize\"

        set mon [lrange \$expect_out(1,string) 5 5]
        puts \"Month.......: \$mon\"

        set day [lrange \$expect_out(1,string) 6 6]
        puts \"Day.........: \$day\"

        set ts [lrange \$expect_out(1,string) 7 7]
        puts \"Timestamp...: \$ts\"

        send \"exit\r\"
        close
    " >> $COPYCHK

    echo "" >> $COPYCHK
}

PerformUpdate ( )
#--------------------------------------------------------------------------------------------
#
# PerformUpdate performs:
#
#   - obtains the date to append to the hosts file backup
#   - loops through all VMs, performing mv, scp and permission update
#

{
    echo ""
    echo "Hosts file update, all processing on the VMs will be displayed below."
    echo ""
    echo "Processing Stage 1 of 2..."
    echo ""

    echo "-----------------------------------------------------------------------------------------------" >> $LOGFILE
    echo "Start Timestamp....: $START" >> $LOGFILE

    for server in ${servers[@]}; do
        echo "VM-$j Results" >> $LOGFILE
        CreateHostsFile
        SendHostsFile
        PermsCopyFile
        GetTmpFileData "copychk"
        echo "+++++++==============================================+++++++"
        echo "VM-$j"
        echo -e "\t- Created hosts file\n\t- Secure File Copy executed\n\t- Performed permission changes (chown/chgrp/chgmod)\n\t- Copied /etc/host to /etc/hosts.$START"

        let "j=j+1"
    done

    END=`date +%Y-%m-%d-%H:%M`
    echo "Finish Timestamp...: $END" >> $LOGFILE
    echo "-----------------------------------------------------------------------------------------------" >> $LOGFILE

    echo ""
    echo "Stage 1 of 2 completed..."
    echo ""

    j=11

    BEGIN=`date +%Y-%m-%d-%H:%M`
    echo "Start Timestamp...: $BEGIN" >> $LOGFILE

    echo -e "Processing Stage 2 of 2...\n"

    for server in ${servers[@]}; do
        echo "VM-$j Results" >> $LOGFILE
        MoveHostsFile
        GetTmpFileData "movechk"
        echo "" >> $LOGFILE
        echo "VM-$j"
        echo "+++++++==============================================+++++++"
        echo -e "\t- Processed move of hosts file"
        let "j=j+1"
    done

    echo ""
    echo "Stage 2 of 2 Completed..."
    echo ""
    echo "Program finished..."

    END=`date +%Y-%m-%d-%H:%M`
    echo "Finish Timestamp...: $END" >> $LOGFILE
    echo "-----------------------------------------------------------------------------------------------" >> $LOGFILE
}

Cleanup ( )
#--------------------------------------------------------------------------------------------
#
# Cleanup performs:
#
#   - Create a build path to store the hosts files created 
#

{
    mkdir build.$START
    mv $CDIR/build/* $CDIR/build.$START   
}

CheckDirPaths ( )
#--------------------------------------------------------------------------------------------
#
# CheckDirPaths performs:
#
#   - Checks the existence of the build and log directory paths.  Should they not
#     exist then they are created. 
#

{
    if [ ! -d $CDIR/build ]
    then
        mkdir $CDIR/build
    fi

    if [ ! -d $CDIR/log ]
    then
        mkdir $CDIR/log
    fi

    rm -f *.out
}

NotifyEmail ( )
#--------------------------------------------------------------------------------------------
#
# NotifyEmail performs:
#
#   - Email SaaS that /etc/hosts on VMs 11-44 will be updated. 
#
{
    /usr/bin/expect -c "

    log_user 0
    set timeout -1

    spawn telnet 209.228.128.121 25

    expect {
        \"^Trying*\"
    }

    send \"ehlo mail.criticalpath.net\r\"

    expect {
        \"*SIZE*\"
    }

    send \"mail from: <HOSTSUPD@333269-vm27.PSO.nsabcp.com>\r\"

    expect {
        \"*FROM*\"
    }

    send \"$RCPTTO\r\"

    expect {
        \"*RCPT*\"
    }

    send \"data\r\"

    expect {
        \"*mail*\"
    }

    send \"From: <HOSTUPD@333269-vm27.PSO.nsabcp.com>\r\"

    expect {
        \"*OSTUP*\"
    }

    send \"$TOGroup\r\"

    expect {
        \"\r\"
    }

    send \"Subject: /etc/hosts Update\r\"

    expect {
        \"\r\"
    }

    send \"Content-Type: text/html; charset="ISO-8859-1"\r\"

    expect {
        \"\r\"
    }

    send \"\r\"

    expect {
        \"\r\"
    }

    send \"The /etc/hosts files will be updated on VMs 11-44 within the next 6-8 minutes.\r\"

    expect {
        \"\r\"
    }

    send \"An email notification will be sent upon completion of this procedure.\r\"

    expect {
        \"\r\"
    }

    send \"\r\"

    expect {
        \"\r\"
    }

    send \".\r\"

    expect {
         \".\r\"
    }

    sleep 5

    expect {
         \"*accepted*\"
    }

    send \"quit\r\"

    expect {
         \"*QUIT*\"
    }

    exit
    "
}

ConfirmEmail ( )
#--------------------------------------------------------------------------------------------
#
# ConfirmEmail performs:
#
#   - Email SaaS that /etc/hosts on VMs 11-44 has been completed. 
#
{
    /usr/bin/expect -c "

    log_user 0
    set timeout -1

    spawn telnet 209.228.128.121 25

    expect {
        \"^Trying*\"
    }

    send \"ehlo mail.criticalpath.net\r\"

    expect {
        \"*SIZE*\"
    }

    send \"mail from: <HOSTSUPD@333269-vm27.PSO.nsabcp.com>\r\"

    expect {
        \"*FROM*\"
    }

    send \"$RCPTTO\r\"

    expect {
        \"*RCPT*\"
    }

    send \"data\r\"

    expect {
        \"*mail*\"
    }

    send \"From: <HOSTUPD@333269-vm27.PSO.nsabcp.com>\r\"

    expect {
        \"*OSTUP*\"
    }

    send \"$TOGroup\r\"

    expect {
        \"\r\"
    }

    send \"Subject: /etc/hosts Update\r\"

    expect {
        \"\r\"
    }

    send \"Content-Type: text/html; charset="ISO-8859-1"\r\"

    expect {
        \"\r\"
    }

    send \"\r\"

    expect {
        \"\r\"
    }

    send \"The update to the /etc/hosts files on VMs 11-44 has completed, thanks for your patience.\r\"

    expect {
        \"\r\"
    }

    send \"\r\"

    expect {
        \"\r\"
    }

    send \".\r\"

    expect {
         \".\r\"
    }

    sleep 5

    expect {
         \"*accepted*\"
    }

    send \"quit\r\"

    expect {
         \"*QUIT*\"
    }

    exit
    "
}

CompareCopyMove ( )
{
    diff $CDIR/copychk.out $CDIR/movechk.out > comp.out

    DF=$(ls -la comp.out)
    DFSIZE=$(echo $DF | awk '{print $5 }') 

    if [ $DFSIZE -eq 0 ]
    then
        echo ""
        echo ""
        echo -e "\t* * * * * * * * * * * * * * * * * * * * * * * * * *"
        echo -e "\t*                                                 *"
        echo -e "\t*   The operation has completed without errors.   *"
        echo -e "\t*                                                 *"
        echo -e "\t* * * * * * * * * * * * * * * * * * * * * * * * * *"
        echo ""
        echo ""
        rm -f *.out
    else
        echo ""
        echo -e "\tPlease review the logfile ./log.$START, along with the "
        echo -e "\tcopychk.out and movechk.out for more detail."
        echo ""
    fi
}

#--------------------------------------------------------------------------------------------
#
# Command line arguments
#
#

username=$1	# use -> amesoner
password=$2	# use -> m3m0va99
destnpath=$3	# use -> /home/amesoner
hostspath=$4	# use -> /etc
 
#--------------------------------------------------------------------------------------------
#
# Global Variables
#
#

START=`date +%Y-%m-%d-%H:%M`
LOGFILE="./log/log.$START"
CDIR=`pwd`
HOSTNAME="$CDIR/build/hosts"
COPYCHK=""
hostsname="xxx"					# change to hosts before real-time use

TOGroup="To: <david.davis@cp.net>"		# used for testing
RCPTTO="rcpt to: <david.davis@cp.net>"		# used for testing

#TOGroup="To: <saas.support@cp.net>"		# real-time use
#RCPTTO="rcpt to: <saas.support@cp.net>"	# real-time use

vm11=192.168.104.101
vm12=192.168.104.102
vm13=192.168.104.103
vm14=192.168.104.104
vm15=192.168.104.105
vm16=192.168.104.106
vm17=192.168.104.107
vm18=192.168.104.108
vm19=192.168.104.109
vm20=192.168.104.110

vm21=192.168.104.111
vm22=192.168.104.112
vm23=192.168.104.113
vm24=192.168.104.114
vm25=192.168.104.115
vm26=192.168.104.116
vm27=192.168.104.117
vm28=192.168.104.118
vm29=192.168.104.119
vm30=192.168.104.5

vm31=192.168.104.6
vm32=192.168.104.7
vm33=192.168.104.8
vm34=192.168.104.9
vm35=192.168.104.10
vm36=192.168.104.11
vm37=192.168.104.12
vm38=192.168.104.13
vm39=192.168.104.14
vm40=192.168.104.15

vm41=192.168.104.16
vm42=192.168.104.17
vm43=192.168.104.161
vm44=192.168.104.162

servers=("$vm11"  "$vm12" "$vm13" "$vm14" "$vm15" \
         "$vm16" "$vm17" "$vm18" "$vm19" "$vm20" \
         "$vm21" "$vm22" "$vm23" "$vm24" "$vm25" \
         "$vm26" "$vm27" "$vm28" "$vm29" "$vm30" \
         "$vm31" "$vm32" "$vm33" "$vm34" "$vm35" \
         "$vm36" "$vm37" "$vm38" "$vm39" "$vm40" \
         "$vm41" "$vm42" "$vm43" "$vm44" )

j=11

#--------------------------------------------------------------------------------------------
#
# MAIN
#
#

NotifyEmail
CheckDirPaths
PerformUpdate
Cleanup
CompareCopyMove
ConfirmEmail

echo ""
echo ""
