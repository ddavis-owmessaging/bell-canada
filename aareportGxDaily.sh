#!/bin/bash

source /tordir01/gx/.profile

mydate=`TZ=aaa24 date +%Y%m%d`

/tordir01/gx/bin/aareport -r all -csv -date  $mydate > /var/tmp/EdgeReport/aareports_`date +%Y%m%d`.csv
#/tordir01/gx/bin/aareport -r all -csv -date  20090708 > /var/tmp/EdgeReport/aareports_20090708.csv
sleep 15
cat /dev/null > /tordir01/gx/scripts/irftpdebug.out
cat /dev/null > /tordir01/gx/scripts/idrmail.scr
cd /var/tmp/EdgeReport
chmod 755 aareports_`date +%Y%m%d`.csv
chmod u+x /tordir01/gx/scripts/idrmail.scr
MAIL="/tordir01/gx/scripts/idrmail.scr"
MAILDEBUG="/tordir01/gx/scripts/idrmaildebug.out"
#SEND="uuencode /var/tmp/EdgeReport/aareports_`date +%Y%m%d`.csv aareports_`date +%Y%m%d`.csv | mailx -s 'AAREPORT DAILY REPORT - `date +%Y%m%d` ' marouane.balmakhtar@openwave.com john.reali@openwave.com duy.doan@openwave.com mike.mohammed@openwave.com"

#FTP START
#Mike Added for FTP upload
rm /var/tmp/EdgeReport/upload/*
cp /var/tmp/EdgeReport/aareports_`date +%Y%m%d`.csv /var/tmp/EdgeReport/upload/
ftp -v ftp.openwave.com </tordir01/gx/scripts/ftp.scr >/tmp/ftpout
mailx -s "AAREPORT DAILY REPORT sent to ftp server from $HOSTNAME" aliant-asp-ops@owmessaging.com </tmp/ftpout
#FTP DONE

SEND="uuencode /var/tmp/EdgeReport/aareports_`date +%Y%m%d`.csv aareports_`date +%Y%m%d`.csv | mailx -s 'AAREPORT DAILY REPORT - `date +%Y%m%d` ' aliant-asp-mon@owmessaging.com kevin.cuba@owmessaging.com"
#SEND="uuencode /var/tmp/EdgeReport/aareports_20090708.csv aareports_`date +%Y%m%d`.csv | mailx -s 'AAREPORT DAILY REPORT - `date +%Y%m%d` ' marouane.balmakhtar@owmessaging.com"
echo "$SEND" >> $MAIL
#$MAIL >> $MAILDEBUG 2>&1
$MAIL >> $MAILDEBUG
