#!/bin/bash

source /tordir01/gx/.profile
#output=/var/tmp/EdgeReport/aaoffenders_outbound_`date +%m%d%Y%H%M`.csv

#mytime=`TZ=aaa24 date +%m%d%Y%H%M`

#####prev_hour=`TZ=GMT+5 date +%H`
prev_hour=`TZ=GMT+6 date +%H`
curr_hour=`date +%H`
report_date=`TZ=GMT+6 date +%m"/"%d"/"%Y`
label_report_date=`TZ=GMT+6 date +%m%d%Y`

#####report_date=`TZ=GMT+5 date +%m"/"%d"/"%Y`
#####label_report_date=`TZ=GMT+5 date +%m%d%Y`

output="/var/tmp/EdgeReport/aaoffenders_outbound_"$label_report_date"_"$prev_hour":00-"$curr_hour":00.csv"

echo "$report_date start: $prev_hour:00 end: $curr_hour:00" > $output
echo " " >> $output


/tordir01/gx/bin/aaoffenders  -all -nodns -group gx-out -top 50 -prevhours 3 |tr '[:blank:]' '[,]' | grep -v "192.168.64.160" | grep -v "192.168.64.8" | grep -v "207.154.59.124" | grep -v "127.0.0.1" | grep -v "192.168.68.135" | grep -v "192.168.68.136" | grep -v "192.168.68.137" | grep -v "192.168.68.138" | grep -v "192.168.68.139" | grep -v "192.168.66.135" | grep -v "192.168.66.136" | grep -v "192.168.66.137" | grep -v "192.168.66.138" | grep -v "192.168.66.139">> $output
sleep 25

cat /dev/null > /tordir01/gx/scripts/ooffmaildebug.out
cat /dev/null > /tordir01/gx/scripts/ooffmail.scr
cd /var/tmp/EdgeReport
chmod u+x /tordir01/gx/scripts/ooffmail.scr
chmod 760 $output
#HOSTNAME="ftp.openwave.com"
#USERID="aliant"
#PASSWORD="Want/start!"
#MODE=bin
#REMOTEPATH="/drop/reports/edge"
#FILE="aaoffenders_outbound_`date +%m%d%Y%H%M`.csv"
#DEBUG="/tordir01/gx/scripts/iftpdebug.out"
#FTPSCR="/tordir01/gx/ftpedgereports.scr"
#echo "open $HOSTNAME" > $FTPSCR
#echo "user $USERID $PASSWORD" >> $FTPSCR
#echo "prompt" >> $FTPSCR
#echo "$MODE" >> $FTPSCR
#echo "cd $REMOTEPATH" >> $FTPSCR
#echo "put $FILE" >> $FTPSCR
#echo "bye" >> $FTPSCR
#ftp -n -v < $FTPSCR >> $DEBUG 2>&1
#echo "`date +%m/%d/%Y-%H:%M`  aaoffenders done....." > /tmp/iaaoffenders.done

MAIL="/tordir01/gx/scripts/ooffmail.scr"
MAILDEBUG="/tordir01/gx/scripts/ooffmaildebug.out"
#SEND="uuencode $output aaoffenders_outbound_`date +%m%d%Y%H%M`.csv | mailx -s 'OUTBOUND AAOFFENDERS HOURLY REPORT - `date +%m%d%Y%H%M`' aliant-asp-ops@owmessaging.com abuse@aliant.ca"

#Original - 11/18/2009
SEND="uuencode $output "aaoffenders_outbound_"$label_report_date"_"$prev_hour":00-"$curr_hour":00.csv" | mailx -s 'OUTBOUND AAOFFENDERS HOURLY REPORT - $report_date start: $prev_hour:00 end: $curr_hour:00' aliant-asp-ops@owmessaging.com"

SEND="cat $output | mailx -s 'OUTBOUND AAOFFENDERS HOURLY REPORT - $report_date start: $prev_hour:00 end: $curr_hour:00' aliant-asp-mon@owmessaging.com"

#FTP START
#Mike Added for FTP upload
rm /var/tmp/EdgeReport/upload/*
cp $output /var/tmp/EdgeReport/upload/
#####ftp -v ftp.openwave.com </tordir01/gx/scripts/ftp.scr >/tmp/ftpout
##mailx -s "OUTBOUND AAOFFENDERS HOURLY REPORT sent to ftp server from $HOSTNAME" aliant-asp-ops@owmessaging.com </tmp/ftpout
#FTP DONE

size=`ls -la $output | awk -F" " '{print $5}'`

if [ $size -eq 0 ]
then
        echo "##########`date`###########" >> /var/tmp/gx_outbound_size_check.log
        echo "Size was 0. BB will be alarmed." >> /var/tmp/gx_outbound_size_check.log
        echo `ls -la $output` >> /var/tmp/gx_outbound_size_check.log
        echo "RED" > /tmp/bb_gx_outbound

else
        echo "##########`date`###########" >> /var/tmp/gx_outbound_size_check.log
        echo "Size is not 0. All Clear." >> /var/tmp/gx_outbound_size_check.log
        echo `ls -la $output` >> /var/tmp/gx_outbound_size_check.log
        echo "GREEN" > /tmp/bb_gx_outbound

fi



#SEND="uuencode /var/tmp/EdgeReport/aaoffenders_outbound_`date +%m%d%Y%H%M`.csv aaoffenders_outbound_`date +%m%d%Y%H%M`.csv | mailx -s 'OUTBOUND AAOFFENDERS HOURLY REPORT - `date +%m%d%Y%H%M`' duy.doan@owmessaging.com"
echo "$SEND" >> $MAIL
#$MAIL >> $MAILDEBUG 2>&1
$MAIL >> $MAILDEBUG
exit
