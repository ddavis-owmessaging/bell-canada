#!/bin/bash

source /tordir01/gx/.profile

mydate=`TZ=aaa24 date +%Y%m%d`

/tordir01/gx/bin/aareport -r all -group gx-in -csv -date  $mydate > /var/tmp/EdgeReport/aareportsinbound_$mydate.csv
sleep 15
cat /dev/null > /tordir01/gx/scripts/irftpdebug.out
cat /dev/null > /tordir01/gx/scripts/idrmail.scr
cd /var/tmp/EdgeReport
chmod 755 aareportsinbound_$mydate.csv
chmod u+x /tordir01/gx/scripts/idrmail.scr
MAIL="/tordir01/gx/scripts/idrmail.scr"
MAILDEBUG="/tordir01/gx/scripts/idrmaildebug.out"

#FTP START
#Mike Added for FTP upload
rm /var/tmp/EdgeReport/upload/*
cp /var/tmp/EdgeReport/aareportsinbound_$mydate.csv /var/tmp/EdgeReport/upload/
ftp -v ftp.openwave.com </tordir01/gx/scripts/ftp.scr >/tmp/ftpout
mailx -s "DAILY INBOUND AAREPORT sent to ftp server from $HOSTNAME" aliant-asp-ops@openwave.com </tmp/ftpout
#FTP DONE

SEND="uuencode /var/tmp/EdgeReport/aareportsinbound_$mydate.csv aareportsinbound_$mydate.csv | mailx -s 'DAILY INBOUND AAREPORT - $mydate ' aliant-asp-mon@openwave.com kevin.cuba@openwave.com"
echo "$SEND" >> $MAIL
#$MAIL >> $MAILDEBUG 2>&1
$MAIL >> $MAILDEBUG
