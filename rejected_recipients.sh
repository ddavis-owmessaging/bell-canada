#!/bin/bash

date=`date +%Y%m%d`
date2=`date +%Y%m%d%H%M`
reject_input=/var/tmp/reject_$date.out
reject_raw=/var/tmp/reject_send_ip.out
reject_count=/var/tmp/reject_sender_count.out
reject_count2=/var/tmp/reject_ip_count.out
output=/home/reports/rejected/reject_recipients_$date2.out

for i in 01 02 03 04 05
do
	cat /imail/logs/current/tormtz$i/log/mta.tormtz$i.$date*.log | fgrep -i "Note;MtaRecipientsRejected" | grep "fromhost="  >> $reject_input
done

outputdate=`date +%Y%m%d" "%H":"%M`

/usr/local/scripts/reject_parser.pl $reject_input


cat $reject_raw | sort | uniq -c | sort -k 1 -nr | head -20 > $reject_count
cat $reject_raw | awk -F" " '{print $3}' | sort | uniq -c | sort -k 1 -nr | head -20 > $reject_count2

echo "TOP 20 Senders with Rejected Recipients as of $outputdate" > $output
echo " " >> $output
echo " Number of Messages   Mail Address   Destination Host   IP Address" >> $output
echo " " >> $output
cat $reject_count >> $output
echo " " >> $output
echo "#####################################################################################" >> $output
echo " " >> $output
echo " TOP 20 Rejected Senders by IP." >> $output
echo " " >> $output
cat $reject_count2 >> $output


cat $output | mailx -s "ALIANT - Todays TOP 20 Senders with Rejected Recepients total as of $outputdate" aliant-asp-mon@owmessaging.com

rm $reject_input
rm $reject_raw
rm $reject_count
rm $reject_count2
#rm $output

