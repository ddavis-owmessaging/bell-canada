#!/bin/bash

source /tordir01/gx/.profile

mydate=`TZ=aaa24 date +%Y%m%d`
###reportdate=`TZ=aaa24 date +%m%d%Y`
reportdate=`TZ=aaa24 date +%Y%m%d`

output="/var/tmp/EdgeReport/aaoffenders_outbound_$reportdate.csv"
upload_dir=/var/tmp/EdgeReport/upload_outbound/
log=/var/tmp/gx_outbound_size_check.log


/tordir01/gx/bin/aaoffenders -all -nodns -group gx-out -top 50 -date $mydate | tr '[:blank:]' '[,]' | grep -v "192.168.64.160" | grep -v "192.168.64.8" | grep -v "207.154.59.124" | grep -v "127.0.0.1" | grep -v "192.168.68.135" | grep -v "192.168.68.136" | grep -v "192.168.68.137" | grep -v "192.168.68.138" | grep -v "192.168.68.139" | grep -v "192.168.66.135" | grep -v "192.168.66.136" | grep -v "192.168.66.137" | grep -v "192.168.66.138" | grep -v "192.168.66.139" > $output


#Mail Send
cat $output | mailx -s "OUTBOUND AAOFFENDERS DAILY TOP 50 REPORT - $mydate" aliant-asp-ops@owmessaging.com

#FTP section
cd $upload_dir
rm *
cp $output $upload_dir
ftp -v ftp.openwave.com </tordir01/gx/scripts/ftp_outbound.scr >/tmp/ftpout

#Check size section

size=`ls -la $output | awk -F" " '{print $5}'`
if [ $size -eq 0 ]
then
	echo "##########`date`###########" >> $log
	echo "Size was 0. BB will be alarmed." >> $log
	echo `ls -la $output` >> $log
	echo "RED" > /tmp/bb_gx_outbound

else
	echo "##########`date`###########" >> $log
	echo "Size is not 0. All Clear." >> $log
	echo `ls -la $output` >> $log
	echo "GREEN" > /tmp/bb_gx_outbound

fi
