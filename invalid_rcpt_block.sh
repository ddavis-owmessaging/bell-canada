#!/bin/bash

################################################
#                                              #
# Script By: Mike Mohammed                     #
# Modified: 03122010 - added whitelist         #
#                                              #
################################################

date=`date +%Y%m%d`
block_output=/var/tmp/block_output.out
block_output2=/var/tmp/block_output2.out
lock=/var/tmp/block_output.lck 
block_accts=/var/tmp/block_acct_list.txt
block_accts2=/var/tmp/block_acct_list2.txt
transfer_file=/var/tmp/new_block_accts_before_sort.txt
transfer_file_send=/var/tmp/new_block_accts.txt
outfile=/home/opwv/imail/scripts/logs/invalid_rcpt_block.log
outfile_rpt=/home/opwv/imail/scripts/nohup.out
whitelist=/home/opwv/imail/scripts/invalid_rcpt_block.whitelist

defer_notice=/var/tmp/deferred_notice.txt
defer_list=/var/tmp/defferred_list.txt

rm $block_output
rm $block_output2
rm $transfer_file
rm $transfer_file_send
rm $outfile_rpt

date >> $outfile
echo " " >> $outfile

if [ -f $lock ]
then
	echo "Lock File exists exiting..." >> $outfile
        exit
else
	touch $lock
        if [ -f $lock ]
        then
		for x in 01 02 03 04 05
		do
        		cat /imail/logs/current/tormtz$x/log/mta.tormtz$x.$date*.log | fgrep -i "MtaRecipientsRejected" >> $block_output
		done

		for y in 01 02 03 04 05
		do
			cat /imail/logs/current/tormtz$y/log/mta.tormtz$y.$date*.log | fgrep -i "deferred:user" >> $block_output2
		done

		accts=`grep "MtaRecipientsRejected (created by Error-Handler)" $block_output | nawk -F":" '{print $2}'| sort | grep -v localAddr |grep -v "<>"| uniq -c | sort -k 1 -nr | awk '$1 > 50 {print}'| awk -F"< " '{print $2}'| awk -F">" '{print $1}'` 

		accts2=`cat $block_output2 | nawk -F":" '{print $3}'| sort | grep -v localAddr |grep -v "<>"| uniq -c | sort -k 1 -nr | awk '$1 > 200 {print}'| awk -F"< " '{print $2}'| awk -F">" '{print $1}'`

		cat $block_accts | sort | uniq > $block_accts2
		
		echo "Accounts to Block:" >> $outfile
		echo "Analyzing the Recipients Rejected List:" >> $outfile
		echo "$accts" >> $outfile
		for acct in $accts
		do 	
			grep $acct $block_accts2
			if [ $? = 0 ]
                        then
				echo " " >> $outfile
				echo "Account $acct Exists in Block List - moving to next..." >> $outfile
			else
				grep $acct $whitelist
				if [ $? = 0 ]
				then
					echo " " >> $outfile
					echo "Account $acct Exists in White List - moving to next..." >> $outfile
				else
					echo " " >> $outfile
					echo "Account $acct is New - blocking account..." >> $outfile
					echo "$acct" >> $block_accts
					echo "$acct" >> $transfer_file
					echo "*** $acct Added to tordir01 processing file..." >> $outfile
					#Automate Report Sending
					rm $outfile_rpt
					nohup /home/opwv/imail/scripts/invalid_rcpt_email.sh -e $acct >> $outfile_rpt
					cat $outfile_rpt | mailx -s "Invalid Recipients - $acct" aliant-asp-mon@owmessaging.com
				fi
			fi
		done

		echo " " >> $outfile
		echo "Analyzing the Deferred List:" >> $outfile
                echo "$accts2" >> $outfile
                for acct2 in $accts2
                do     
                        grep $acct2 $block_accts2
                        if [ $? = 0 ]
                        then
                                echo " " >> $outfile
                                echo "Account $acct2 Exists in Block List - moving to next..." >> $outfile
                        else
                                echo " " >> $outfile
                                echo "Account $acct2 is New - blocking account..." >> $outfile
                                echo "$acct2" >> $block_accts
				echo "$acct2" >> $defer_list
                               ## echo "$acct2" >> $transfer_file
                               # echo "*** $acct Added to tordir01 processing file..." >> $outfile
                        fi
                done

		if [ -f $defer_list ]
		then
			echo "Please investigate the following accounts that have exceeded more than 200 deferred recipients:" >> $defer_notice
			echo " " >> $defer_notice
   			cat $defer_list >> $defer_notice
			cat $defer_notice | mailx -s "ACCOUNT INVESTIGATION - DEFERRED RECIPIENTS" aliant-asp-mon@owmessaging.com
			rm -rf $defer_list
			rm -rf $defer_notice
		fi

		if [ -f $transfer_file ]
		then
			cat $transfer_file | sort | uniq > $transfer_file_send
			count=`cat $transfer_file_send | wc -l`
			if [ $count -ge 10 ]
			then
				mailx -s "REJECT IP SCRIPT: Too many accounts to block. Please investigate" aliant-asp-mon@owmessaging.com < /dev/null
				rm $lock
				exit
			else
				scp $transfer_file_send imail@tordir01:/tmp
			fi
		fi
	else
		rm $lock
                exit	
	fi
fi

rm $lock
echo " " >> $outfile
echo "Exiting...." >> $outfile
echo "-------------------------------------------" >> $outfile
echo " " >> $outfile
echo " " >> $outfile
